﻿Shader "Custom/WobbleShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_WobbleSpeed("WobbleSpeed", Range(0,5)) = 0.75
		_WobbleAmplitude("WobbleAmplitude", Range(0,1)) = 0.05
		[Toggle] _doEmission("Do Emission?", Float) = 0
		[HDR] _Emission("Emission", Color) = (0,0,0,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert

		#include "UnityCG.cginc"

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
		float _WobbleSpeed;
		float _WobbleAmplitude;
		half3 _Emission;
		float _doEmission;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
			if (_doEmission == 1) {
				o.Emission = _Emission;
			}
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }

		void vert(inout appdata_base v) {
			v.vertex.x += v.vertex.x * sin(_Time.w * _WobbleSpeed) * _WobbleAmplitude;
			v.vertex.y += v.vertex.y * sin(_Time.w * _WobbleSpeed - 90) * _WobbleAmplitude;
			v.vertex.z += v.vertex.z * sin(_Time.w * _WobbleSpeed - 180) * _WobbleAmplitude;
		}

        ENDCG
    }
    FallBack "Diffuse"
}
