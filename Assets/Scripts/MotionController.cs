﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionController : MonoBehaviour {

    public GameObject cube;
    public float dragSensitivity = 0.1f;
    public float forceSensitivity = 20f;

    private new Rigidbody rigidbody;
    private Vector3 mouseReference;
    private Quaternion cubeRotation;

    void Start() {
        rigidbody = cube.GetComponent<Rigidbody>();
        Application.targetFrameRate = 60;
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            rigidbody.angularVelocity = Vector3.zero;

            // store mouse
            mouseReference = Input.mousePosition;

            //store cube rot.
            cubeRotation = cube.transform.rotation;

        } else if (Input.GetMouseButtonUp(0)) {
            Vector3 mouseOffset = -(Input.mousePosition - mouseReference) * dragSensitivity;

            rigidbody.AddTorque(new Vector3(0, mouseOffset.x * forceSensitivity, 0));
        }

        if (Input.GetMouseButton(0)) {
            // offset
            Vector3 mouseOffset = (Input.mousePosition - mouseReference) * dragSensitivity;

            // rotate
            cube.transform.Rotate(new Vector3(0, 0, -(mouseOffset.x + mouseOffset.y)));

            // store mouse
            mouseReference = Input.mousePosition;
        }
    }

}
